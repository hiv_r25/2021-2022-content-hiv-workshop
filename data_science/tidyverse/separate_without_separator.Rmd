---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Tidydata"
author: "Josh Granek"
date: "October 29, 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
```

## `tidyr` for tidying data

`separate` requires a separator. How do you split columns that have structure but no separator? Let's make the `desc` column without `-` and see . . .
```{r}
set.seed(1)
n <- 4
df <- tibble(
    pid = c(1,3,4,5),
    desc = paste(sample(c('M', 'F'), n, replace=T),
                    sample(1:15, n),
                    sep=''),
    visit1 = rpois(n, lambda = 20),
    visit2 = rpois(n, lambda=10)
)

df[3,3] = NA

df
```
### Pivot_longer

Problem 1: Each row has **2** observations.
```{r}
df %>% 
    pivot_longer(cols = c(visit1,visit2), 
                 names_to = "visit", values_to = "measurement")
```
### Extract: when there is no separator!
Here we use `extract` instead of `separate`. `extract` allows us to use a regular expression to describe the different "groups" of information that combine to make the composite value.In the case of `desc`:

1. the first part is the sex of the individual, specified as "M" or "F", which is matched by `[MF]` - this says "we are looking for a single "M" or "F".

2. The second part is the age of the individual, specified by one or more numerals, immediately after the "M" or "F". This is matched by `\\d+` - this says "we are looking for one or more numerals".

Each of the groups (which will become the columns) is specified by a pair of parentheses, which is why our final regex is "([MF])(\\d+)"
```{r}
desc_regex="([MF])(\\d+)"
df %>% 
pivot_longer(cols = c(visit1,visit2), 
                 names_to = "visit", values_to = "measurement") %>%
    extract(desc, 
            regex=desc_regex,
            into=c("sex", "age"))
```




