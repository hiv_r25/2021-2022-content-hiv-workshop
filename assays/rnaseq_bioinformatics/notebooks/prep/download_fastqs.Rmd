---
title: "Setup for RNA-Seq"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(here)
library(dplyr)
here("assays/rnaseq_bioinformatics/notebooks/config.R") %>%
  source
```

```{bash eval=FALSE, include=FALSE}
set -u
chmod -R u+w $DATA_DIR
ddsclient download -p HTS_course --include hts_2019_data/hts2019_pilot_rawdata/21_2019_P_M1_S21_L002_R1_001.fastq.gz $DATA_DIR
chmod -R a-w $DATA_DIR
```

```{bash}
set -u
chmod -R u+w $DATA_DIR
ddsclient download -p HTS_course --include hts_2019_data/hts2019_pilot_rawdata $DATA_DIR
chmod -R a-w $DATA_DIR
```