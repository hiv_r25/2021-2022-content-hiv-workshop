library(here)
library(fs)
library(dplyr)

# Common
scratch_dir="/work"
data_dir="/hpc/group/chsi-hiv-r25-2022/data"
fastq_dir=file.path(data_dir,"hts_2019_data","hts2019_pilot_rawdata")

num_cpus=16
Sys.setenv(DATA_DIR=data_dir)
Sys.setenv(NUM_CPUS=num_cpus)

Sys.setenv(RAW_FASTQS=fastq_dir)

username=Sys.info()[["user"]]
out_dir=file.path(scratch_dir,username,"hiv2022","rnaseq")
Sys.setenv(OUT_DIR=out_dir)

# --------------------
# fastqc.Rmd
# --------------------
qc_dir=file.path(out_dir,"qc_dir")
Sys.setenv(QC_DIR=qc_dir)

# --------------------
# fastq_trimming.Rmd
# --------------------
trim_dir=file.path(out_dir,"trim_dir")
Sys.setenv(TRIM_DIR=trim_dir)

here("assays/rnaseq_bioinformatics/info") ->
  info_dir
adapter_fasta=file.path(info_dir, "neb_e7600_adapters.fasta")
Sys.setenv(ADAPTER_FASTA=adapter_fasta)

# --------------------
# genome_prep.Rmd
# --------------------
genome_dir=file.path(out_dir,"genome_dir")
Sys.setenv(GENOME_DIR=genome_dir)

star_dir=file.path(out_dir,"star_dir")
Sys.setenv(STAR_DIR=star_dir)

gtf_url="ftp://ftp.ensemblgenomes.org/pub/release-39/fungi/gtf/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99/Cryptococcus_neoformans_var_grubii_h99.CNA3.39.gtf.gz"
fasta_url="ftp://ftp.ensemblgenomes.org/pub/release-39/fungi/fasta/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99/dna/Cryptococcus_neoformans_var_grubii_h99.CNA3.dna.toplevel.fa.gz"

gtf_url %>%
  basename %>%
  file.path(genome_dir,.) ->
  gtf_gz_path

fasta_url %>%
  basename %>%
  file.path(genome_dir,.) ->
  fasta_gz_path

fasta_gz_path %>%
  path_ext_remove ->
  fasta_path

gtf_gz_path  %>%
  path_ext_remove ->
  gtf_path

Sys.setenv(FASTA=fasta_path)
Sys.setenv(GTF=gtf_path)

#-----------------------
# DESeq analysis
#-----------------------
count_dir=file.path(data_dir,"hts_2019_data","hts2019_pilot_counts")
metadata_tsv=file.path(fastq_dir,"2019_pilot_metadata.tsv")
