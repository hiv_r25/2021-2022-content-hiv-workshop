---
title: Trimming and Filtering a FASTQ
output: html_document
---

# Setup
## Load Libraries

```{r}
library(fs)
library(here)
library(dplyr)
library(readr)
```

## Assign the variables in this notebook.
Retyping shell variables in every notebook is getting old, and its error prone.  Let's centralize these so we can share them between notebooks.  We can create a shell script that contains the shell variables that we need, and then we can `source` it in each notebook.  Let's call it `config.R`.  We can do this using the RStudio text editor.

```{r}
"assays/rnaseq_bioinformatics/notebooks/config.R" %>%
    here() %>%
    source
```

## Making New Directories
Make the directories that are new in this notebook
```{r}
dir_create(trim_dir)
```


Now let's check to be sure that worked.  We will run `ls` and check that these directories now exist in the `$CUROUT` directory.
```{r}
file_info(trim_dir)
```

# Trimming and Filtering
Now we get into some actual preprocessing.  We will use `fastq-mcf` to trim adapter from our reads and do some quality filtering.  We need to trim adapter, because if a fragment is short enough, we will sequence all the way through the fragment and into the adapter.  Obviously the adapter sequence in not found in the genome, and can keep the read from aligning properly.  

## Adapter file
To do the trimming, we need to generate an adapter file. To save time, we will be using an adapter file that I generated in advance. Details of how to generate an adapter file are in make_adapter_fasta.Rmd in this repository.

Here are the contents of the adapter file
```{r}
read_file(adapter_fasta) %>%
    cat
```
## fastq-mcf
You can run `fastq-mcf -h` to get details about running fastq-mcf.  We will adjust run parameters, because some of the defaults set a low bar (even the author acknowleges this).

```{bash error=TRUE}
# the "| cat" is a hack that prevents problems with jupyter
fastq-mcf -h
```

### Running fastq-mcf
1. neb_e7600_adapters.fasta : the adapter file
2. 27_MA_P_S38_L002_R1_001.fastq.gz : the FASTQ with the data (fastq-mcf, like most NGS analysis software, detects gzipped files and automatically decompresses on the fly)
3. -q 20 : if a read has any bases with quality score lower than this, trim them and anything 3' of that base
4. -x 0.5 : if this percentage (or higher) of the reads have an "N" in a given position, trim all reads to that position
5. -o 27_MA_P_S38_L002_R1_001.trim.fastq.gz : output file (the .gz ending tells fastq-mcf to compress the output file)

```{bash}
set -u
fastq-mcf $ADAPTER_FASTA \
    $RAW_FASTQS/21_2019_P_M1_S21_L002_R1_001.fastq.gz \
    -q 20 -x 0.5 \
    -o $TRIM_DIR/21_2019_P_M1_S21_L002_R1_001.trim.fastq.gz
```
check that trimmed file is there
```{r}
list.files(trim_dir, full.names=TRUE )
```

at this point we could run fastqc on the output of fastq-mcf to see if statistics have improved, but we will skip that for now.




