# Computing Environment
  - [Introduction to Open OnDemand](ondemand_for_rnaseq.md)
  - [Cloning the Gitlab Repository](../../misc/git_cloning.md)
  - Bash Intro
    - [Setup for SWC Unix Lessons](../../misc/unix_shell_setup.Rmd)
    - [Software Carpentry: The Unix Shell](https://swcarpentry.github.io/shell-novice/)
      
# RNA-Seq Bioinformatic Steps
  - [Quality Control](notebooks/fastqc.Rmd)
  - [Trimming and Filtering](notebooks/fastq_trimming.Rmd)
  - [Download and Index Genome](notebooks/genome_prep.Rmd)
  - [Mapping Reads to a Reference Genome](notebooks/mapping.Rmd)
    
# Appendix
  - [Make Adapter File](notebooks/make_adapter_fasta.Rmd)
  - [Quality Score Explanation](notebooks/quality_scores.Rmd)
  - [Run Full Pipeline](notebooks/run_everything.Rmd)