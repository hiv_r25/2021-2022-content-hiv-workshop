## scRNA-Seq
1. [Background Slides](slides/scrna_background.pdf)
1. [Bioinformatic Analysis: PBMC 1k](notebooks/cellranger_count_pbmc1k.Rmd)
1. [Statistical Analysis Slides](slides/scRNAseq_Analysis_Primer_2022-04-06.pdf)
1. [Statistical Analysis](notebooks/scRNA-seq_tutorial-seurat-version.rmd)

## Appendix
1. [Configuration File](notebooks/config.R)
2. [Download Data](notebooks/setup/download_10x_data.Rmd)
