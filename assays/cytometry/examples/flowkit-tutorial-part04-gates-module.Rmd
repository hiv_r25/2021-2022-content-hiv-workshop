---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```{r}
library(reticulate)
use_python("/usr/bin/python3")
options(reticulate.repl.quiet = TRUE)
```

# FlowKit Tutorial - Part 4 - The gates Module and Supporting Classes

https://flowkit.readthedocs.io/en/latest/?badge=latest

Part 4 of the tutorial series covers the programmatic construction of a `GatingStrategy`. This will require using the Gate sub-classes in the `gates` module as well as a few supporting classes used in FlowKit.

If you have any questions about FlowKit, find any bugs, or feel something is missing from these tutorials [please submit an issue to the GitHub repository here](https://github.com/whitews/FlowKit/issues/new/).

## Table of Contents

* [`Dimension` Class](#Dimension-Class)
* [`Vertex` Class](#Vertex-Class)
* [`gates` Module](#gates-Module)
  * [`RectangleGate` Class](#RectangleGate-Class)
  * [`PolygonGate` Class](#PolygonGate-Class)
  * [`EllipsoidGate` Class](#EllipsoidGate-Class)
  * [`QuadrantGate` Class](#QuadrantGate-Class)
  * [`BooleanGate` Class](#BooleanGate-Class)

```{python}
import bokeh
from bokeh.plotting import show
import matplotlib.pyplot as plt

import flowkit as fk

bokeh.io.output_notebook()
# %matplotlib inline

_ = plt.ioff()
```

```{python}
# check version so users can verify they have the same version/API
fk.__version__
```

## `Dimension` Class

Before we cover how gates are created in FlowKit, we'll first need to go over a few supporting classes necessary to create them. The first of which is the `Dimension` class, which is a reference to a specific channel in one or more Sample instances, and specifies the preprocessing intructions for that channel. The `Dimension` class is modelled after the "dimension" XML element used in GatingML-2.0 and in FlowJo 10 workspaces.

Let's look at the constructor:

    Dimension(
        dimension_id, 
        compensation_ref='uncompensated', 
        transformation_ref=None, 
        range_min=None, 
        range_max=None
    )

The **`dimension_id`** corresponds to a Sample PnN label of a channel. The **`compensation_ref`** is a string referencing a Matrix `id`. The values 'uncompensated' or 'fcs' are reserved for events in a gate that should not be compensated ('uncompensated') or events that should be compensated using the spillover matrix found in the FCS sample ('fcs'). The **`transformation_ref`** is a string referencing the `id` of a `Transform` sub-class. `None` specifies the channel data should not be transformed. 

Finally, we have the `range_min` and `range_max` arguments. These are only used for `Dimension` instances intended for use with a `RectangleGate`. It is admittedly a bit odd, but this is how the GatingML-2.0 specification and FlowJo 10 workspaces implement channel dimension references and rectangle / range gates. We'll go over these values in more detail in the `RectangleGate` section. 

```{python}
help(fk.Dimension)
```

## `Vertex` Class

A `Vertex` instance represents a single vertex of a polygon. The constructor is simple, consisting of a single `coordinates` argument of a tuple of 2 float values. A collection of 3 or more `Vertex` instances are used to define the boundary of a `PolygonGate`

```{python}
help(fk.Vertex)
```

## `gates` Module

The `gates` module contains classes related to the various gate types available within FlowKit. These include:

* RectangleGate
* PolygonGate
* EllipsoidGate
* QuadrantGate (and the related Quadrant class)
* BooleanGate

We'll cover each of these gate types below, but first let's load our simple diamond FCS to use for demonstration, review the channels, and plot the events. We'll also make an empty `GatingStrategy` instance to add and apply our gates.

```{python}
sample = fk.Sample("data/simple_diamond_example/test_data_diamond_01.fcs")
```

```{python}
sample.channels
```

```{python}
f = sample.plot_scatter('channel_A', 'channel_B', source='raw')
show(f)
```

```{python}
chan_a_idx = sample.get_channel_index('channel_A')
events_a = sample.get_channel_events(chan_a_idx, 'raw')
```

```{python}
events_a.shape
```

```{python}
events_a.min(), events_a.max()
```

```{python}
g_strat = fk.GatingStrategy()
```

### `RectangleGate` Class

The `RectangleGate` represents a GatingML-2.0 rectangle gate, which covers rectangles (2-D), hyper-rectangles (>2-D), and the "range" gate (1-D). A RectangleGate must have one or more dimensions, and each dimension must specify at least one of a minimum or maximum value (or both).

In the 1-D case, a single `Dimension` instance must be provided and include one or both of a `range_min` or `range_max` value. If only one is given the range is open-ended, with the minimum range being inclusive of the min value, and the maximum range being exclusive of the max value. For example, if only a `range_min` of 1000 is provided, the gate will include events >= `range_min`. If only a `range_max` value is provided, the gate will include events < `range_max`.

In the 2-D case, the same rules apply. Given 2 `Dimension` instances, the gate can still be open-ended in one of each of the dimensions. Only if both `min_range` and `max_range` are provided in both dimensions will a true rectangular gate be defined. The same logic applies for the hyper-rectangle in 3 or more dimensions.

```{python}
help(fk.gates.RectangleGate)
```

Let's create an open-ended `RectangleGate` to inlude the top left side of the diamond

```{python}
# neither dimension needs compensation or transformation, so we'll use the defaults for those arguments
dim_a = fk.Dimension('channel_A', range_max=50000)
dim_b = fk.Dimension('channel_B', range_min=50000)
```

```{python}
rect_top_left_gate = fk.gates.RectangleGate('top-left', parent_gate_name=None, dimensions=[dim_a, dim_b])
```

```{python}
g_strat.add_gate(rect_top_left_gate)
```

The gate has been added to our gating strategy, and we expect ~25% of the events to be included within the gate. Let's apply the gate to our sample and look at the results.

```{python}
res = g_strat.gate_sample(sample)
```

```{python}
res.report
```

And we have ~25%, the extra 1 event is due to the inclusive limit of the `range_min` value in `dim_b`.


### `PolygonGate` Class

The `PolygonGate` represents a GatingML-2.0 polygon gate, which is a closed polygon defined in exactly 2 dimensions. The vertices of the `PolygonGate` are defined by a list of `Vertex` instances.

```{python}
help(fk.gates.PolygonGate)
```

Let's make a child `PolygonGate` under the previous `RectangleGate`, then add to the gating strategy and re-process our `Sample`. 

```{python}
v1 = fk.Vertex((25000, 75000))
v2 = fk.Vertex((50000, 75000))
v3 = fk.Vertex((50000, 100000))
v4 = fk.Vertex((25000, 100000))

vertices = [v1, v2, v3, v4]

poly_gate = fk.gates.PolygonGate(
    'poly1', 
    parent_gate_name='top-left', 
    dimensions=[dim_a, dim_b],
    vertices=vertices
)
```

```{python}
g_strat.add_gate(poly_gate)
res = g_strat.gate_sample(sample)
```

```{python}
res.report
```

### `EllipsoidGate` Class

The `EllipsoidGate` represents a GatingML-2.0 ellipsoid gate, which is an ellipsoid defined in 2 or more dimensions. To define an `EllipsoidGate`, we must specify the ellipsoid's mean value (center of the ellipsoid), its covariance matrix (defining the shape and orientation), and a distance square (the square of the Mahalanobis distance, defining its size)

```{python}
help(fk.gates.EllipsoidGate)
```

Let's define a 2-D ellipse at the root level around the right corner of of the diamond. 

```{python}
center = (100000, 50000)
cov = [[5000**2, 0], [0, 5000**2]]
dist = 1

ellipse_gate = fk.gates.EllipsoidGate(
    'ellipse1', 
    parent_gate_name=None, 
    dimensions=[dim_a, dim_b], 
    coordinates=center,
    covariance_matrix=cov,
    distance_square=dist
)
```

```{python}
g_strat.add_gate(ellipse_gate)
res = g_strat.gate_sample(sample)
```

```{python}
res.report
```

### `QuadrantGate` Class

The `QuadrantGate` represents a GatingML-2.0 quadrant gate. Quadrant gates are different from other gate types in that they are actually a collection of gates (quadrants), though even the term quadrant is misleading as they can divide a plane into more than 4 sections. A `QuadrantGate` must have at least 1 divider, and must specify the label of the resulting quadrants the dividers produce.

To construct a `QuadrantGate`, we need to introduce to new helper classes: the `QuadrantDivider` and the `Quadrant`. We'll see how to put it all together below to make a `QuadrantGate` that divides the diamond into its 4 sides.

**Note: When retrieving a single Quadrant from the GatingStrategy method `get_gate`, the owning QuadrantGate will be returned. A single quadrant isn't technically a gate and has no parent reference.**

```{python}
help(fk.gates.QuadrantGate)
```

```{python}
# QuadrantDivider instances are similar a Dimension, they take compensation_ref and tranformation_ref
# arguments. However, we'll use the defaults here of 'uncompensated' & None.
quad_div1 = fk.QuadrantDivider(
    'chan-a-div',
    'channel_A',
    compensation_ref='uncompensated', 
    transformation_ref=None, 
    values=[50000]
)
quad_div2 = fk.QuadrantDivider(
    'chan-b-div', 
    'channel_B',
    compensation_ref='uncompensated', 
    transformation_ref=None, 
    values=[50000]
)
quad_divs = [quad_div1, quad_div2]

# the 2 dividers above will be used to divide the space into 4 quadrants
quad_1 = fk.gates.Quadrant(
    quadrant_id='chanApos-chanBpos',
    divider_refs=['chan-a-div', 'chan-b-div'],
    divider_ranges=[(50000, None), (50000, None)]
)
quad_2 = fk.gates.Quadrant(
    quadrant_id='chanApos-chanBneg',
    divider_refs=['chan-a-div', 'chan-b-div'],
    divider_ranges=[(50000, None), (None, 50000)]
)
quad_3 = fk.gates.Quadrant(
    quadrant_id='chanAneg-chanBpos',
    divider_refs=['chan-a-div', 'chan-b-div'],
    divider_ranges=[(None, 50000), (50000, None)]
)
quad_4 = fk.gates.Quadrant(
    quadrant_id='chanAneg-chanBneg',
    divider_refs=['chan-a-div', 'chan-b-div'],
    divider_ranges=[(None, 50000), (None, 50000)]
)
quadrants = [quad_1, quad_2, quad_3, quad_4]

# We can now construct our QuadrantGate
quad_gate1 = fk.gates.QuadrantGate(
    'quadgate1', 
    parent_gate_name=None, 
    dividers=quad_divs, 
    quadrants=quadrants
)
```

```{python}
g_strat.add_gate(quad_gate1)
res = g_strat.gate_sample(sample)
```

```{python}
res.report
```

### `BooleanGate` Class

The `BooleanGate` represents a GatingML-2.0 Boolean gate, and performs the boolean operations AND, OR, or NOT on one or more other gates. Note, the boolean operation XOR is not supported in the GatingML specification but can be implemented using a combination of the supported operations.

Let's create a boolean gate from our ellipse and top right quadrant of the quadrant gate.

```{python}
help(fk.gates.BooleanGate)
```

```{python}
gate_refs = [
    {
        'ref': 'ellipse1',
        'path': ('root',),
        'complement': False
    },
    {
        'ref': 'chanApos-chanBpos',
        'path': ('root', 'quadgate1'),
        'complement': False
    }
]

bool_gate = fk.gates.BooleanGate('And1', None, 'and', gate_refs)
```

```{python}
g_strat.add_gate(bool_gate)
res = g_strat.gate_sample(sample)
```

```{python}
res.report
```

```{python}

```
