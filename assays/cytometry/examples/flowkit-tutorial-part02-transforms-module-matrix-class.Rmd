---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```{r}
library(reticulate)
use_python("/usr/bin/python3")
options(reticulate.repl.quiet = TRUE)
```

# FlowKit Tutorial - Part 2 - The `transforms` Module & `Matrix` Class

https://flowkit.readthedocs.io/en/latest/?badge=latest

Part 1 of tutorial notebook series introduced the Sample class. In part 2, we will demonstrate how to transform Sample event data for better visualization and analysis, as well as how to compensate the fluorescent channel data for spillover.

Explanations for why compensation and transformation is necessary for analysis are beyond the scope of this tutorial. There are many resources available online covering these topics, including the [Wikipedia page on flow cytometry bioinformatics](https://en.wikipedia.org/wiki/Flow_cytometry_bioinformatics).

If you have any questions about FlowKit, find any bugs, or feel something is missing from these tutorials [please submit an issue to the GitHub repository here](https://github.com/whitews/FlowKit/issues/new/).

## Table of Contents

* [`transforms` Module](#transforms-Module)
  * [Create our Sample](#Create-our-Sample)
  * [Plot Sample untransformed](#plot-untransformed)
  * [Plot LogicleTransform](#plot-logicle)
  * [Plot AsinhTransform](#plot-asinh)
  * [Plot WSPBiexTransform](#plot-biex)
  * [Notes on the other Transform sub-classes](#notes-on-other-xforms)
* [Exercise to develop an insight for FCM transforms](#insight-on-xforms)
* [Using transforms without a Sample](#transforms-standalone)
* [`Matrix` Class](#Matrix-Class)
  * [Creating a Matrix from CSV](#create-matrix)
  * [Retrieve Matrix array as a Pandas DataFrame](#matrix-as-df)
  * [Apply Matrix to a Sample](#apply-matrix)
  * [Modifying a Matrix](#Modifying-a-Matrix)


```{python}
import bokeh
from bokeh.plotting import show
import matplotlib.pyplot as plt
import numpy as np

import flowkit as fk

bokeh.io.output_notebook()
# %matplotlib inline

_ = plt.ioff()
```

```{python}
# check version so users can verify they have the same version/API
fk.__version__
```

## `transforms` Module

The `transforms` module contains classes related to the various transformation of flow cytometry data. The classes include:

* `AsinhTransform`
* `HyperlogTransform`
* `LinearTransform`
* `LogicleTransform`
* `LogTransform`
* `RatioTransform`
* `WSPBiexTransform`
* `WSPLogTransform`

The most common transforms used in FCM data are the "logicle", "biex", and "asinh" transforms. The logicle transform (available as the `LogicleTransform` class) was created specifically for FCM data. In the flow community, the term "biex" transform typically refers to the default transform in the FlowJo application (and is now the default in FlowJo 10). The `WSPBiexTransform` replicates the FlowJo implementation of this transform. All transform classes beginning with "WSP" are FlowJo compatible transforms, the other being the `WSPLogTransform` which is a FlowJo variant of the standard `LogTransform`. In recent years, the inverse hyperbolic sine (asinh) has gained in popularity and is available in FlowKit as the `AsinhTransform`.

We will compare the output of these transforms and cover their input parameters in this notebook.  


### Create our Sample

We'll use an 8-color FCS file for the examples in this notebook. First, let's create our Sample instance and review the channels. Remember from part 1, the Sample is sub-samples by default (for better plot performance).

```{python}
fcs_path = 'data/8_color_data_set/fcs_files/101_DEN084Y5_15_E01_008_clean.fcs'
```

```{python}
sample = fk.Sample(fcs_path)
```

```{python}
sample.channels
```

<!-- #region -->
<a id="plot-untransformed"></a>
#### Let's plot the CD3 channel versus our live/dead marker without transforming


*Note: We can use the channel numbers (not indices) for easy reference to channels*
<!-- #endregion -->

```{python}
p = sample.plot_scatter(12, 10, source='raw', subsample=True)
show(p)
```

<a id="plot-logicle"></a>
#### Without a transformation it is difficult to visualize the different cell sub-populations. Let's try the logicle transform and re-plot the data

```{python}
logicle_xform = fk.transforms.LogicleTransform('logicle', param_t=262144, param_w=0.5, param_m=4.5, param_a=0)
sample.apply_transform(logicle_xform)

p = sample.plot_scatter(12, 10, source='xform', subsample=True)
show(p)
```

#### That's much better, but what are all those parameters? We'll check the help docs for the transform...

The 1st argument is the ID for the transform, which we will use in part 4 of the tutorial notebook series. The ID is used to easily reference which transform we want to use for a particular gate. Below we'll look at the documentation for the `LogicleTransform` to get a better understanding of the input parameters. 

Reading the documentation is helpfu, but experimenting with the input parameters can give a more intuitive understanding of what each of them does.

```{python}
help(fk.transforms.LogicleTransform)
```

<a id="plot-asinh"></a>
#### Let's compare to the AsinhTransform

```{python}
asinh_xform = fk.transforms.AsinhTransform('asinh', param_t=262144, param_m=4.0, param_a=0.0)
sample.apply_transform(asinh_xform)

p = sample.plot_scatter(12, 10, source='xform', subsample=True)
show(p)
```

The main difference between the logicle and the asinh transforms is the inclusion of the extra "w" parameter in the logicle transform, which controls the number of decades in the linear region. The asinh transform has no such parameter. Notice we reduced the "m" parameter from 4.5 (logicle) to 4.0 (asinh) in an attempt to create a closer output between them. Since asinh has no linear parameter the total decades was reduced by the amount of the linear region used in the logicle. 


<a id="plot-biex"></a>
#### Let's move on to the FlowJo biex transform

The FlowJo biex transform has similar input parameters, with some subtle but important differences. Details about these parameters from the FlowJo docs are included below. Note, the FlowJo interface does not allow changing the max value parameter, I believe it is set automatically depending on the FCS data range.

> Adjusting width:
The value for w will determine the amount of channels to be compressed into linear space around zero.  The space of linear does not change, but rather the number of channels or bins being compressed into the linear space.
> 
> Width should be set high enough that all of the data in the histogram is visible on screen, but not so high that extra white space is seen to the left hand side of your dimmest distribution. For most practical uses, once all events have been shifted off the axis and there is no more axis ‘pile-up’, then the optimal width basis value has been reached.
>
> Negative:
Another component in the biexponential transform calculation is the negative decades or negative space. This is the only other value you will probably ever need to adjust.  In cases where a high width basis may start compressing dim events into the negative cluster, you may want to lower the width basis (less compression around zero) and instead, increase the negative space by 0.5 – 1.0.  Doing this will expand the space around zero so the dim events are still visible, but also expand the negative space to remove the cells from the axis and allow you to see the full distribution.
>
> Positive:
The presence of the positive decade adjustment is due to the algorithm used for logicle transformation, but is not useful in 99.9% of the cases that require adjusting the biexponential transform.  It may be appropriate to adjust this value only if you use data that displays data with a data range greater than 5 decades.

```{python}
# Note: these are the default values in FlowJo (and in FlowKit, so you don't have to remember these specific values)
biex_xform = fk.transforms.WSPBiexTransform('biex', max_value=262144.000029, positive=4.418540, width=-10, negative=0)
sample.apply_transform(biex_xform)

p = sample.plot_scatter(12, 10, source='xform', subsample=True)
show(p)
```

<!-- #region -->
<a id="notes-on-other-xforms"></a>
### Notes on the lesser used transform classes

* `LinearTransform`
  
  The linear transform linearly scales the data. When plotted, the axes values will be different but the plotted events will look the same as untransformed data.
  
* `LogTransform`
  
  The logarithmic transform works well for visualizing high intensity event values, but becomes problematic as we approach zero. Low intensity values are affected by the binning of the log function (esp. from >0 to 100). The log  function is only defined for x > 0, so any zero or negative event values become undefined. It is for these reasons, alternative transforms were created to visualize and analyze FCM data.

* `HyperlogTransform`

  The hyperlog is another transform created specifically for FCM data, though is not used as often in recent years. More information about hyperlog can be found in the original manuscript:
  
  C. Bruce Bagwell.  Hyperlog – A flexible log-like transform for negative, zero, and positive valued data.  Cytometry part A, Volume 64A, Issue 1, pages 34-42, March 2005.


* `RatioTransform`
  
  The ratio transform is defined in the GatingML-2.0 specification, and is basically a parametrized ratio of 2 channels. It is included in FlowKit for full compatibility with GatingML-2.0. 
  
* `WSPLogTransform`
  
  FlowJo implements a custom version of the logarithmic function, and it is included in FlowKit for better compatibility with FlowJo workspaces. If you plan on using a log transform and exporting to a FlowJo workspace from FlowKit, you must use this transform.
<!-- #endregion -->

<a id="insight-on-xforms"></a>
### Exercise to develop an insight for FCM transforms

The "hybrid" linear-log transforms commonly used to display FCM data can be difficult to grasp. FlowKit includes a synthetic FCS file with 2 channels where the events have been arranged in a diamond pattern. This file is used internally for test purposes, but may also be helpful for some users to gain a better intuition for how these hybrid transforms work. Let's load the diamond sample and plot the events before and after a logicle transform.

```{python}
diamond_sample = fk.Sample("data/simple_diamond_example/test_data_diamond_01.fcs")
```

```{python}
diamond_sample.channels
```

```{python}
f = diamond_sample.plot_scatter(1, 2, source='raw')
show(f)
```

```{python}
diamond_sample.apply_transform(logicle_xform)
f = diamond_sample.plot_scatter(1, 2)
show(f)
```

We can see in the transformed plot how events in the lower range get distributed more widely, allowing better visualization of any cell populations that may be concentrated there.


<a id="transforms-standalone"></a>
## Using `Transform` sub-classes without a Sample

For cases where event data has been extracted using FlowKit or was derived from a source other than an FCS file, and needs to be transformed, the `Transform` sub-classes can be used independent of a `Sample` instance. The API for all the `Transform` sub-classes (except the `RatioTransform` class) provide `apply` and `inverse` methods that both take NumPy arrays as input and output transformed data, also as NumPy arrays.

Below is an example of transforming a NumPy array using the `LogicleTransform`, but all the `Transform` sub-classes work the same way. 

```{python}
data = np.arange(0, 100000, 100000/20).reshape(2, 10).T
```

```{python}
data
```

```{python}
xform = fk.transforms.LogicleTransform('asinh', param_t=95000., param_w=0.5, param_m=4.5, param_a=0)
```

```{python}
xform_data = xform.apply(data)
```

```{python}
xform_data
```

```{python}
inv_xform_data = xform.inverse(xform_data)
```

```{python}
inv_xform_data
```

## `Matrix` Class

So far, we have only transformed our sample for better visualization. However, it is often necessary for flow cytometry data to be compensated to correct for fluorescence "spillover" between fluorescent channels. FlowKit supports compensation via the `Matrix` class.

The `Matrix` class represents a compensation matrix used for correcting spillover in fluorescence channels. A compensation matrix (also called a spillover matrix) is applied prior to transforming events, however fluorescent events generally need to be transformed in order to verify if the compensation matrix is adequate or needs to be modified. This is somewhat confusing, but makes sense if you remember that visualizing fluorescent events without a transform is very difficult to interpret, as we saw above. The good news is that FlowKit will handle the order of compensation and transformation operations for you.

A `Matrix` instance can easily be created from a variety of data sources, including:

* a file path, file handle, or Path object to a CSV/TSF file
* a NumPy array of spill data
* a Pandas DataFrame (channel labels as headers)

Below we show the help docs for the Matrix class, let's look at the constructor:

```
Matrix(
    matrix_id, 
    spill_data_or_file, 
    detectors, 
    fluorochromes=None, 
    null_channels=None
)
```

The `matrix_id`, like the previous `transform_id` for the `Transform` sub-classes, is used to easily reference which compensation matrix we want to use for a particular gate. However, there are a few reserved values that cannot be used when creating a `Matrix` instance. The values 'uncompensated' or 'fcs' are reserved for events in a gate that should not be compensated ('uncompensated') or events that should be compensated using the spillover matrix found in the FCS sample ('fcs').

```{python}
help(fk.Matrix)
```

<a id="create-matrix"></a>
### Let's create a `Matrix` instance from a CSV file made for the `Sample` we've been using

*Note: we need to provide the detector labels (i.e. the PnN values for the channels) in the order they appear as columns in the matrix data source.*

Our comp matrix file happens to have columns in the order the fluoro channels appear in the FCS file, so we can just get the fluoro PnN labels as follows:

```{python}
detectors = [sample.pnn_labels[i] for i in sample.fluoro_indices]
```

```{python}
detectors
```

```{python}
comp_file_path = 'data/8_color_data_set/den_comp.csv'
```

```{python}
comp_mat = fk.Matrix(
    'my_spill',
    comp_file_path,
    detectors
)
```

<a id="matrix-as-df"></a>
#### We can retrieve the `Matrix` as a Pandas DataFrame or review or use outside of FlowKit

```{python}
comp_mat.as_dataframe()
```

<a id="apply-matrix"></a>
#### Now, apply the matrix to our sample (our previous transform will be re-applied automatically)

```{python}
sample.apply_compensation(comp_mat)
```

```{python}
p = sample.plot_scatter(12, 10, source='xform', subsample=True)
show(p)
```

### Modifying a Matrix

If the compensation is not adequate and needs adjusting, the underlying NumPy array of the Matrix class can be modified directly or a new Matrix instance can be created from a DataFrame. Let's load a new Sample and use the compensation matrix from the $SPILL keyword. We will see this cytometer-created matrix needs adjusting and demonstrate how to modify the Matrix.

```{python}
fcs_path = 'data/100715.fcs'
sample = fk.Sample(fcs_path, subsample=20000)  # subsample to 20k events
```

```{python}
sample.channels
```

```{python}
# Apply logicle transform and plot the CD3 vs CD8 channels (uncompensated)
xform = fk.transforms.LogicleTransform('logicle', param_t=262144, param_w=0.5, param_m=4.5, param_a=0)
sample.apply_transform(xform)

fig = sample.plot_scatter(5, 8, source='xform', subsample=True)
show(fig)
```

```{python}
# Apply the matrix from the 'spill' keyword and re-plot
sample.apply_compensation(sample.metadata['spill'])

fig = sample.plot_scatter(5, 8, source='xform', subsample=True)
show(fig)
```

#### The default comp matrix doesn't look too good, let's edit it

We can easily get the loaded compensation, which is a Matrix instance. The Matrix method `as_dataframe` also takes an optional argument `fluoro_labels` to display the fluoro labels rather than the detectors.

```{python}
comp_mat = sample.compensation
```

```{python}
# By default, the columns/rows have detector labels
comp_mat.as_dataframe()
```

```{python}
# Let's switch to use the more human-readable fluorescent labels and save
comp_df = comp_mat.as_dataframe(fluoro_labels=True)
```

```{python}
comp_df
```

```{python}
# Review the CD3/CD8 values
print(comp_df.loc['CD3']['CD8'], comp_df.loc['CD8']['CD3'])
```

```{python}
# Let's change these
comp_df.loc['CD3']['CD8'] = 0.30
comp_df.loc['CD8']['CD3'] = 0.01
```

#### Create a new Matrix with our customized comp values, then re-apply to our Sample

```{python}
comp_mat_modified = fk.Matrix(
    'custom_spill', 
    comp_df.values,  # use new values
    comp_mat.detectors,  # use old detectors
    fluorochromes=comp_mat.fluorochomes  # use old fluoros
)

sample.apply_compensation(comp_mat_modified)

fig = sample.plot_scatter(5, 8, source='xform', subsample=True)
show(fig)
```

#### That's better, probaly needs some adjustment in other channels, but the process would be same.

```{python}

```
