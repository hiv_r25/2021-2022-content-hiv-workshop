---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```{python}
import os
import pandas as pd
import bokeh
from bokeh.plotting import gridplot, show

import flowkit as fk

bokeh.io.output_notebook()
# %matplotlib inline
```

### Load FlowJo Workspace

```{python}
base_dir = "data/8_color_data_set"

sample_path = os.path.join(base_dir, "fcs_files")
wsp_path = os.path.join(base_dir, "8_color_ICS.wsp")
```

```{python}
session = fk.Session(sample_path)
session.import_flowjo_workspace(wsp_path)
```

```{python}
sample_ids = sorted(session.get_sample_ids())
```

```{python}
sample_ids
```

```{python}
session.get_sample_groups()
```

###  Choose sample group, review gate hierarchy, and analyze samples

```{python}
sample_grp = 'DEN'
```

```{python}
print(session.get_gate_hierarchy(sample_grp, 'ascii'))
```

```{python}
session.analyze_samples(sample_grp)
```

### Compare CD8+ gate between all 3 samples

```{python}
plots = []
gate_name = 'CD8+'
x_min = y_min = 0.1
x_max = y_max = 0.9

for s_id in sample_ids:
    p = session.plot_gate(sample_grp, s_id, gate_name, x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)
    plots.append(p)
```

```{python}
gp = gridplot([plots], width=300, height=300)
```

```{python}
show(gp)
```

### Let's modify the 3rd sample's gate (101_DEN084Y5_15_E05_010_clean.fcs)

```{python}
cust_s_id = '101_DEN084Y5_15_E05_010_clean.fcs'
```

```{python}
# before we modify, we'll just review the CD8+ gate stats for this sample
report = session.get_group_report(sample_grp)
report[(report['sample'] == cust_s_id) & (report['gate_name'] == gate_name)]
```

```{python}
# get the sample specific gate & review the vertex instances
s_custom_gate = session.get_gate(sample_grp, gate_name, sample_id=cust_s_id)

verts = s_custom_gate.vertices
```

```{python}
verts
```

```{python}
# the vertex around (0.28, 0.29) could be moved up and to the left a bit
v4 = verts[4]
v4
```

```{python}
v4.coordinates = [0.25, 0.35]
```

```{python}
plots = []
gate_name = 'CD8+'
x_min = y_min = 0.1
x_max = y_max = 0.9

for s_id in sample_ids:
    p = session.plot_gate(sample_grp, s_id, gate_name, x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)
    plots.append(p)

gp = gridplot([plots], width=300, height=300)
show(gp)
```

```{python}
session.analyze_samples(sample_grp)
```

```{python}
# and verify the CD8+ gate stats after the modification
report = session.get_group_report(sample_grp)
report[(report['sample'] == cust_s_id) & (report['gate_name'] == gate_name)]
```

```{python}

```
