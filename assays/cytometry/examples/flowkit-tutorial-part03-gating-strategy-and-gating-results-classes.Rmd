---
jupyter:
  jupytext:
    text_representation:
      extension: .Rmd
      format_name: rmarkdown
      format_version: '1.2'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```{r}
library(reticulate)
use_python("/usr/bin/python3")
options(reticulate.repl.quiet = TRUE)
```

# FlowKit Tutorial - Part 3 - The `GatingStrategy` & `GatingResults` Classes

https://flowkit.readthedocs.io/en/latest/?badge=latest

So far, we've seen how to load FCS files using the Sample class and perform basic pre-processing like compensation and transformation for better visualization of event data. In part 3, we will explore using FlowKit for gating Sample event data using the `GatingStrategy` and `GatingResults` classes.

If you have any questions about FlowKit, find any bugs, or feel something is missing from these tutorials [please submit an issue to the GitHub repository here](https://github.com/whitews/FlowKit/issues/new/).

## Table of Contents

* [`GatingStrategy` Class](#GatingStrategy-Class)
  * [The Gate ID Concept](#gate-id-concept)
  * [Create a GatingStrategy from a GatingMl-2.0 document](#create-gating-strategy-from-gml)
    * [Retrieve the Gate Hierarchy](#retrieve-gate-hierarchy)
    * [Export Gate Hierarchy as Image](#export-hierarchy-as-image)
    * [Retrieve Gate IDs](#retrieve-gate-ids)
    * [Retrieve Gate Instances](#retrieve-gate-instances)
    * [Retrieve Compensation Matrices](#Retrieve-Compensation-Matrices)
    * [Retrieve Transformations](#Retrieve-Transformations)
* [`GatingResults` Class](#GatingResults-Class)
  * [`GatingResults` Report](#GatingResults-Report)

```{python}
import bokeh
from bokeh.plotting import show
import matplotlib.pyplot as plt

import flowkit as fk

bokeh.io.output_notebook()
# %matplotlib inline

_ = plt.ioff()
```

```{python}
# check version so users can verify they have the same version/API
fk.__version__
```

## GatingStrategy Class

A GatingStrategy object represents a collection of hierarchical gates along with the compensation and transformation information referenced by any gate Dimension objects (covered in Part 4 of the tutorial series). A GatingStrategy can be created from a valid GatingML document or built programmatically. Methods in the GatingStrategy class fall in to 3 main categories: adding gate-related objects, retrieving those objects, and applying the gating strategy to a Sample.

Let's look at the documentation for the GatingStrategy class:

```{python}
help(fk.GatingStrategy)
```

<a id="gate-id-concept"></a>
### The Gate ID Concept

Quite a lot of thought has been put into the design of the GatingStrategy class to support the various ways gates are used and processed in typical FCM workflows. The most important concept to understand when interacting with a GatingStrategy instance is how gate IDs are used to reference gates and their position within the gating hierarchy. 

For example, gates are sometimes "re-used" in different branches of the hierarchy, like the same quadrant gate applied to each of the CD4+ and CD8+ populations. Because of this, the name of the gate is not sufficient to fully identify it. Further, simply coupling the gate name with its parent gate name can also become problematic if the nested gates are re-used.

The GatingStrategy class solves this ambiguity by defining a gate ID as a tuple combining the gate name and the full ancestor path of gate names, similar in concept to a computer file system. However, this approach can be cumbersome for the common case where gates are not re-used. Therefore, the GatingStrategy allows for referencing gates simply by their gate name string for cases where that name is not re-used within the gate hierarchy. For ambiguous cases, referencing a gate requires the full gate ID tuple of the gate name and gate path. 

We will see how this works in practice later, but for now let's create a GatingStrategy from an existing GatingML-2.0 document.


<a id="create-gating-strategy-from-gml"></a>
### Create a GatingStrategy from a GatingML-2.0 Document

```{python}
gml_path = '../examples/data/8_color_data_set/8_color_ICS.xml'
g_strat = fk.parse_gating_xml(gml_path)
```

```{python}
g_strat
```

The string representation reveals this GatingStrategy has 6 gates, 3 transforms, and 1 compensation (Matrix instance).


<a id="retrieve-gate-hierarchy"></a>
#### Retrieve the Gate Hierarchy

We can retrieve the gate hierarchy in a variety of formats using the `get_gate_hiearchy` method. The method takes the following `output` options:

* `ascii`: Generates a text-based representation of the gate tree, and is likey the most human-readable for reviewing the hierarchy. This is the default option.
* `json`: Generates a JSON representation of the gate tree, useful for programmatic parsing, especially outside of Python. When this option is used, all extra keywords are passed to `json.dumps` (e.g. `indent=2` works to indent the output).
* `dict`: Generates a Python dictionary representation of the gate tree, useful for programmatic parsing within Python.

```{python}
text = g_strat.get_gate_hierarchy(output='ascii')
```

```{python}
print(text)
```

```{python}
gs_json = g_strat.get_gate_hierarchy(output='json', indent=2)
```

```{python}
print(gs_json)
```

```{python}
gs_dict = g_strat.get_gate_hierarchy(output='dict')
```

```{python}
gs_dict
```

<a id="export-hierarchy-as-image"></a>
#### Exporting Gate Hierarchy as Image (requires the `graphviz` package)

```{python}
g_strat.export_gate_hierarchy_image('gs.png')
```

```{python}
img = plt.imread('gs.png')
```

```{python}
f = plt.figure(figsize=(12, 8))
ax = f.subplots(1)
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

plt.imshow(img)
plt.tight_layout()
plt.show()
```

<a id="retrieve-gate-ids"></a>
#### Retrieve all Gate IDs (with their gate paths)

```{python}
g_strat.get_gate_ids()
```

<a id="retrieve-gate-instances"></a>
#### Retrieve Gate Instances

Below we show retrieving a Gate instance by its gate name, which works here because the name is unambigious within this gate hierarchy. We can also retrieve a gate's parent Gate instance or the list of child gates.

```{python}
g_strat.get_gate('TimeGate')
```

```{python}
g_strat.get_parent_gate('CD3-pos')
```

```{python}
g_strat.get_child_gates('CD3-pos')
```

#### Retrieve Compensation Matrices

```{python}
g_strat.comp_matrices
```

#### Retrieve Transformations

```{python}
g_strat.transformations
```

## GatingResults Class

A GatingResults instance is returned from calling the GatingStrategy `gate_sample` method on a Sample instance, and is never created by an end user directly. A GatingResults instance contains the results of applying the gating hierarchy on a single Sample. Let's load a Sample and apply the previous GatingStrategy via the `gate_sample` method (setting `verbose=True` to print out each gate as it is processed). 

```{python}
sample = fk.Sample("data/8_color_data_set/fcs_files/101_DEN084Y5_15_E01_008_clean.fcs")
```

```{python}
gs_results = g_strat.gate_sample(sample, verbose=True)
```

```{python}
help(gs_results)
```

```{python}
# get the Sample ID for the GatingResults instance
gs_results.sample_id
```

### GatingResults Report

As we can see, the GatingResults class is relatively simple, and it's main purpose is to provide a Pandas DataFrame of the results via the `report` attribute. The report contains a row for every gate and includes the following columns:

* **sample**: the Sample ID of the processed Sample instance
* **gate_path**: tuple of the gate path
* **gate_name**: the name of the gate (or name of the Quadrant of a QuadrantGate)
* **gate_type**: The class name of the gate (RectangleGate, PolygonGate, etc.)
* **quadrant_parent**: Quadrant gates are a bit different, they are really a collection of gates. This field would contain the QuadrantGate name, and each Quadrant name would be in the gate_name field.
* **parent**: the gate name of the parent gate
* **count**: the absolute event count for events inside the gate
* **absolute_percent**: the percentage of events inside the gate relative to the total event count in the Sample
* **relative_percent**: the percentage of events inside the gate relative to the number of events in the parent gate
* **level**: the depth of the gate in the gate tree relative to the root of the tree

```{python}
gs_results.report
```

<a id="gate-membership"></a>
### Retrieve gate membership

The `get_gate_membership` method returns a Boolean array representing which of the Sample events are inside the specified gate.

```{python}
cd3_pos_gate_membership = gs_results.get_gate_membership('CD3-pos')
```

```{python}
cd3_pos_gate_membership
```

```{python}
cd3_pos_gate_membership.sum()
```

We can then use the membership array to retrieve those events from the Sample

**Note: The events we extract here are not necessarily pre-processed the same as they would be given the instructions of the gate, even if using the 'comp' or 'xform' source option.  

```{python}
gated_raw_events = sample.get_events(source='raw')
gated_raw_events = gated_raw_events[cd3_pos_gate_membership]
```

```{python}
gated_raw_events.shape
```

```{python}

```
