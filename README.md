This website can be reached at: https://bit.ly/2YST400

# Workshop Parts
- [Data Science Workshops](data_science/data_science_TOC.md)
- Statistics Workshops
- [Assay Workshops](assays/assay_TOC.md)

# Computing Environments
- [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
    - [Getting started with DCC OnDemand](misc/ondemand_howto.md)
- [Duke Container Manager](https://cmgr.oit.duke.edu/)

# Workshop Content
- [Initial download of workshop content](misc/git_cloning.md)
- [Update workshop content](misc/git_pull.md)
